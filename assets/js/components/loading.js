function initLoading () {
	$('[data-loading]').on('submit', function () {
		var $this = $(this);
		$this.addClass('is-loading')
			.prepend('<div class="spinner">' + $this.data('loading-text') + '</div>');

		 // TODO: remove
		setTimeout(function () {
			window.location.replace("daily-win.html#daily-win");
		}, 1000);
		return false;
	});
}