function initAll () {

	// Enable jQuer UI datepicker if input date type not supported
	if (S('input[type="date"]').prop('type') != 'date' ) {
		S('input[type=date]').datepicker();
	}

	// Kick off every Foundation plugin
	S(document).foundation();

	// Show placeholders fallback
	S('input, textarea').placeholder();

	// Initialize svg4everybody
	svg4everybody();

	// Initialize smooth scrolling
	initSmoothScrolling();

	// Initialize loading
	initLoading();
}


$(function() {
	// Enable FastClick if present
	if (typeof FastClick !== 'undefined') {
		// Don't attach to body if undefined
		if (typeof document.body !== 'undefined') {
			FastClick.attach(document.body);
		}
	}

	initAll();
});